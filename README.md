#Thomas Beavis & Carl Llewellyn

#Pixie Kings

Objective: To defeat the opponent's army and capture their King.

Setup:

    Each player has one King (unused Kings are removed from the game, not put into the deck).
        There is a face down card put on each king that acts as their final line of defence, which can only be attacked when all other cards are killed.
        The king is worth one defending point, so it dies if anything directly attacks it and that player is out.
        Magic cards cannot be used on the king.
    Each player is dealt four cards: one face up, one face down, and two face down, in three separate slots infront of each player, onto the table.
        These will include face cards, however the face cards act as a "mine" where they cannot attack, but if they are attacked they die along with the attacker.
        Players cannot look at the face down cards, but they can be attacked and if the attack fails, they are then left face up.
    Each player also receives three cards in hand.
    The rest of the cards are put into a take pile, which is picked up from in each round.
    There is another discard pile, which all cards are put into when put out of the game.
        These can't be taken from, but are shuffled back into the take pile if the take pile has run out.

Gameplay:

    Only grunts (numbered cards 1-9) can attack, and their power is based on their number.
        When attacking, the higher number wins and the losing card is removed from the game.
        Cards can only be attacked in order, i.e. they cannot attack underneath another card (unless a jester is used to disable the top card, then you can attack underneath).
    When in hand, face cards have unique abilities. When attacked face down in the defending lines, face cards are destroyed along with the attacking card.
    All of these face cards, when used, last for a round, where they are deactivated only if the card they are effecting is destroyed, or if it is the player who used the card's turn again.
    These cards cannot be attacked themselves as they are an "effect", but the card they are affecting can be attacked.
    Face cards:
        The Jack card can disable an opponent's card - this means it cannot be used for the round and a card underneath it can be attacked, it can also still itself be attacked.
        The Queen card, known as the Mistress, allows a player to take control of an opponent's card, where it is taken by them for the round and can be used to attack in the turn it is taken from the oppenent.
            Other players can attack the taken card, if wanted.
            This also becomes a defender for the person who has taken it, so, for example, if this was their last card, their king couldn't be attacked.
        The Ace card halves the power of an opponent's card.
            If the card this is used on is attacked and destroyed, this also is put into the dispose pile.

Turn Order:

    Draw a card (this means four cards in hand on each new turn).
    Replenish your army by putting a grunt card down onto your front line, or attack with a grunt, or play a face card (there is no limit to how many face cards being used). 
        If an attack is unsuccessful, the player's turn ends - no more moves can be made.
        If a player replenishes, their turn is over.
        This means order is important - you have to play magic cards before replenishing or attacking (if the attack fails, you can if it's succesful) - you can't use them after.
        Burn any matching double, triple, or quadriple matching cards, and pick up the amount put down, up to three cards.
    Draw cards until you have three cards in hand.
    Only one card can be replenished per turn.
    A single card can only attack once per turn.

Notes:

    The player can only attack OR replenish in a turn, not both.
    Only one card can be replenished per turn.
    There are three "slots" where cards can be replenished, they have to be face up, only grunts can be used to replenish (1-10 cards) and can only be placed on the face down cards, or an empty slot.
        This means you cannot stack cards ontop of face up cards.
    Face cards can be played after the initial attack, or before.
    If a card attacks and the card it is attacking is the same value, or a face card that hasn't come from the hand, then it is itself killed along with the defender.
    The game finishes when all kings are captured, except one.
    This can be played with four players. Mostly done it with two.
        With more than two players, the rules are the same, it just means you can attack anyone's cards.
        May need some rule changes for more than two players, but don't have enough friends who are nerds where we can figure it out by playing with them haha.
    You have to say, in the style of the narrator guy in the online Halo games:
        "REPLENISH" when replenishing a card.
        "THE MISTRESS" when using the mistress/queen.
        "HALF LIFE" when using the ace.
        "SUICIDE" when a card is itself killed when attacking.
        "DISABLED" when using the jack.
        "KNIGHT CARD" when putting down a ten (it's the most powerful grunt card).